export default [
    {
        header: true,
        title: 'Random Clock',
        hiddenOnCollapse: true
    }
    ,
    {
        title: 'Clock',
        icon: 'fas fa-clock',
        href: '/'
    },
    {
        title: 'Historic Dates',
        icon: 'fas fa-list',
        href: '/history-list'
    }
]